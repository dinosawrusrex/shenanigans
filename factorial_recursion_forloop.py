def factorial_with_recursion(x):
    if x > 1:
        output = x * factorial_with_recursion(x-1)
        return(output)
    else:
        return(1)

print(factorial_with_recursion(3))
print(factorial_with_recursion(5))

def factorial_with_for_loop(x):
    output = x
    for number in range(x-1, 0, -1):
        output *= number
    return(output)

print(factorial_with_for_loop(3))
print(factorial_with_for_loop(5))

