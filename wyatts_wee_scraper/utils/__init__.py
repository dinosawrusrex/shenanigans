import urllib

headers = {"user-agent": "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0"}

request = lambda url: urllib.request.Request(url, headers=headers)


from .links import get_links
from .address import get_details
